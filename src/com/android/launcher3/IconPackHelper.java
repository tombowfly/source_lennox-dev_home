package com.lennox.launcher3;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import com.lennox.launcher3.Launcher;
import com.lennox.launcher3.settings.SettingsProvider;

import com.lennox.utils.FileUtils;
import com.lennox.utils.IntentUtils;
import com.lennox.utils.PackageUtils;
import com.lennox.utils.PlayStoreUtils;

public class IconPackHelper {

    public static final String HOME_THEME_PACK_PACKAGE_NAME = "com.lennox.launcher3.themepack";

    static final String ICON_MASK_TAG = "iconmask";
    static final String ICON_BACK_TAG = "iconback";
    static final String ICON_UPON_TAG = "iconupon";
    static final String ICON_SCALE_TAG = "scale";

    public final static String[] sSupportedActions = new String[] {
        "org.adw.launcher.THEMES",
        "com.gau.go.launcherex.theme"
    };

    public static final String[] sSupportedCategories = new String[] {
        "com.fede.launcher.THEME_ICONPACK",
        "com.anddoes.launcher.THEME",
        "com.teslacoilsw.launcher.THEME",
        "com.lennox.launcher.THEME"
    };

    public static final String[] sLennoxCategories = new String[] {
        "com.lennox.launcher3.THEME"
    };

    // Holds package/class -> drawable
    private Map<String, String> mIconPackResources;
    private final Context mContext;
    private String mLoadedIconPackName;
    private Resources mLoadedIconPackResource;
    private ArrayList<Drawable> mIconBacks, mIconUpons, mIconMasks;
    private static ArrayList<String> iconBackList, iconMaskList, iconUponList;
    private float mIconScale;

    public Drawable getIconBack() {
        return loadRandomIcon(mIconBacks);
    }

    public Drawable getIconMask() {
        return loadRandomIcon(mIconMasks);
    }

    public Drawable getIconUpon() {
        return loadRandomIcon(mIconUpons);
    }

    public float getIconScale() {
        return mIconScale;
    }

    IconPackHelper(Context context) {
        mContext = context;
        mIconPackResources = new HashMap<String, String>();
    }

    private Drawable getDrawableForName(String name) {
        if (isIconPackLoaded()) {
            String item = mIconPackResources.get(name);
            if (!TextUtils.isEmpty(item)) {
                int id = getResourceIdForDrawable(item);
                if (id != 0) {
                    return mLoadedIconPackResource.getDrawable(id);
                }
            }
        }
        return null;
    }

    public static Map<String, IconPackInfo> getSupportedPackages(Context context) {
        Intent i = new Intent();
        Map<String, IconPackInfo> packages = new HashMap<String, IconPackInfo>();
        PackageManager packageManager = context.getPackageManager();
        for (String action : sSupportedActions) {
            i.setAction(action);
            for (ResolveInfo r : packageManager.queryIntentActivities(i, 0)) {
                IconPackInfo info = new IconPackInfo(r, packageManager, "");
                packages.put(r.activityInfo.packageName + "|", info);
            }
        }
        i = new Intent(Intent.ACTION_MAIN);
        for (String category : sSupportedCategories) {
            i.addCategory(category);
            for (ResolveInfo r : packageManager.queryIntentActivities(i, 0)) {
                IconPackInfo info = new IconPackInfo(r, packageManager, "");
                packages.put(r.activityInfo.packageName + "|", info);
            }
            i.removeCategory(category);
        }
        i = new Intent(Intent.ACTION_MAIN);
        for (String category : sLennoxCategories) {
            i.addCategory(category);
            for (ResolveInfo r : packageManager.queryIntentActivities(i, 0)) {
                String appInfo = r.activityInfo.toString();
                String activityName = appInfo.substring(appInfo.lastIndexOf(".") + 1, appInfo.length() - 1);
                IconPackInfo info = new IconPackInfo(r, packageManager, activityName);
                packages.put(r.activityInfo.packageName + "|" + activityName, info);
            }
            i.removeCategory(category);
        }
        return packages;
    }

    private static void loadResourcesFromXmlParser(XmlPullParser parser,
            Map<String, String> iconPackResources) throws XmlPullParserException, IOException {
        int eventType = parser.getEventType();
        do {

            if (eventType != XmlPullParser.START_TAG) {
                continue;
            }

            if (parser.getName().equalsIgnoreCase(ICON_MASK_TAG) ||
                    parser.getName().equalsIgnoreCase(ICON_BACK_TAG) ||
                    parser.getName().equalsIgnoreCase(ICON_UPON_TAG)) {
                String icon = parser.getAttributeValue(null, "img");
                String tag = parser.getName().toLowerCase();
                ArrayList<String> results = new ArrayList<String>();
                if (icon == null) {
                    int i = 1;
                    while (true) {
                        icon = parser.getAttributeValue(null, "img" + String.valueOf(i));
                        if (icon != null) {
                            results.add(tag + String.valueOf(i));
                            iconPackResources.put(tag + String.valueOf(i), icon);
                            i++;
                        } else {
                            break;
                        }
                    }
                }
                if (icon == null && parser.getAttributeCount() == 1) {
                    icon = parser.getAttributeValue(0);
                }
                if (icon != null && results.size() == 0) {
                    results.add(tag);
                    iconPackResources.put(tag, icon);
                }
                if (tag.equals(ICON_MASK_TAG)) {
                    iconMaskList = results;
                } else if (tag.equals(ICON_BACK_TAG)) {
                    iconBackList = results;
                } else if (tag.equals(ICON_UPON_TAG)) {
                    iconUponList = results;
                }
                continue;
            }

            if (parser.getName().equalsIgnoreCase(ICON_SCALE_TAG)) {
                String factor = parser.getAttributeValue(null, "factor");
                if (factor == null) {
                    if (parser.getAttributeCount() == 1) {
                        factor = parser.getAttributeValue(0);
                    }
                }
                iconPackResources.put(parser.getName().toLowerCase(), factor);
                continue;
            }

            if (!parser.getName().equalsIgnoreCase("item")) {
                continue;
            }

            String component = parser.getAttributeValue(null, "component");
            String drawable = parser.getAttributeValue(null, "drawable");

            // Validate component/drawable exist
            if (TextUtils.isEmpty(component) || TextUtils.isEmpty(drawable)) {
                continue;
            }

            // Validate format/length of component
            if (!component.startsWith("ComponentInfo{") || !component.endsWith("}")
                    || component.length() < 16) {
                continue;
            }

            // Sanitize stored value
            component = component.substring(14, component.length() - 1).toLowerCase();

            ComponentName name = null;
            if (!component.contains("/")) {
                // Package icon reference
                iconPackResources.put(component, drawable);
            } else {
                name = ComponentName.unflattenFromString(component);
                if (name != null) {
                    iconPackResources.put(name.getPackageName(), drawable);
                    iconPackResources.put(name.getPackageName() + "." + name.getClassName(), drawable);
                }
            }
        } while ((eventType = parser.next()) != XmlPullParser.END_DOCUMENT);
    }

    private static void loadApplicationResources(Context context,
            Map<String, String> iconPackResources, String packageName) {
        Field[] drawableItems = null;
        try {
            Context appContext = context.createPackageContext(packageName,
                    Context.CONTEXT_INCLUDE_CODE | Context.CONTEXT_IGNORE_SECURITY);
            drawableItems = Class.forName(packageName+".R$drawable",
                    true, appContext.getClassLoader()).getFields();
        } catch (Exception e){
            return;
        }

        for (Field f : drawableItems) {
            String name = f.getName();

            String icon = name.toLowerCase();
            name = name.replaceAll("_", ".");

            iconPackResources.put(name, icon);

            int activityIndex = name.lastIndexOf(".");
            if (activityIndex <= 0 || activityIndex == name.length() - 1) {
                continue;
            }

            String iconPackage = name.substring(0, activityIndex);
            if (TextUtils.isEmpty(iconPackage)) {
                continue;
            }
            iconPackResources.put(iconPackage, icon);

            String iconActivity = name.substring(activityIndex + 1);
            if (TextUtils.isEmpty(iconActivity)) {
                continue;
            }
            iconPackResources.put(iconPackage + "." + iconActivity, icon);
        }
    }

    public ArrayList<Drawable> loadAllIcons(ArrayList<String> pool) {
        if (pool == null || pool.isEmpty()) return null;
        ArrayList<Drawable> icons = new ArrayList<Drawable>();
        for (String icon : pool) {
            icons.add(getDrawableForName(icon));
        }
        return icons;
    }

    public Drawable loadRandomIcon(ArrayList<Drawable> icons) {
        if (icons == null || icons.isEmpty()) return null;
        final Random randomGenerator = new Random();
        return icons.get(randomGenerator.nextInt(icons.size()));
    }

    public boolean loadIconPack(String packageName, String activityName) {
        mIconPackResources = getIconPackResources(mContext, packageName, activityName);
        Resources res = null;
        try {
            res = mContext.getPackageManager().getResourcesForApplication(packageName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        mLoadedIconPackResource = res;
        mLoadedIconPackName = packageName;
        mIconBacks = loadAllIcons(iconBackList);
        mIconMasks = loadAllIcons(iconMaskList);
        mIconUpons = loadAllIcons(iconUponList);
        String scale = mIconPackResources.get(ICON_SCALE_TAG);
        if (scale != null) {
            try {
                mIconScale = Float.valueOf(scale);
            } catch (NumberFormatException e) {
            }
        }
        return true;
    }

    public static Map<String, String> getIconPackResources(Context context, String packageName, String activityName) {
        if (TextUtils.isEmpty(packageName)) {
            return null;
        }
        String appFilter;
        if (activityName != null && !TextUtils.isEmpty(activityName)) {
            appFilter = "appfilter_" + activityName.toLowerCase();
        } else {
            appFilter = "appfilter";
        }

        Resources res = null;
        try {
            res = context.getPackageManager().getResourcesForApplication(packageName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        XmlPullParser parser = null;
        InputStream inputStream = null;
        Map<String, String> iconPackResources = new HashMap<String, String>();

        try {
            inputStream = res.getAssets().open(appFilter + ".xml");
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            parser = factory.newPullParser();
            parser.setInput(inputStream, "UTF-8");
        } catch (Exception e) {
            // Catch any exception since we want to fall back to parsing the xml/
            // resource in all cases
            int resId = res.getIdentifier(appFilter, "xml", packageName);
            if (resId != 0) {
                parser = res.getXml(resId);
            }
        }

        if (parser != null) {
            try {
                  loadResourcesFromXmlParser(parser, iconPackResources);
                  return iconPackResources;
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                // Cleanup resources
                if (parser instanceof XmlResourceParser) {
                    ((XmlResourceParser) parser).close();
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                    }
                }
            }
        }

        // Application uses a different theme format (most likely launcher pro)
        int arrayId = res.getIdentifier("theme_iconpack", "array", packageName);
        if (arrayId == 0) {
            arrayId = res.getIdentifier("icon_pack", "array", packageName);
        }

        if (arrayId != 0) {
            String[] iconPack = res.getStringArray(arrayId);
            for (String entry : iconPack) {

                if (TextUtils.isEmpty(entry)) {
                    continue;
                }

                String icon = entry.toLowerCase();
                entry = entry.replaceAll("_", ".");

                iconPackResources.put(entry, icon);

                int activityIndex = entry.lastIndexOf(".");
                if (activityIndex <= 0 || activityIndex == entry.length() - 1) {
                    continue;
                }

                String iconPackage = entry.substring(0, activityIndex);
                if (TextUtils.isEmpty(iconPackage)) {
                    continue;
                }
                iconPackResources.put(iconPackage, icon);

                String iconActivity = entry.substring(activityIndex + 1);
                if (TextUtils.isEmpty(iconActivity)) {
                    continue;
                }
                iconPackResources.put(iconPackage + "." + iconActivity, icon);
            }
        } else {
            loadApplicationResources(context, iconPackResources, packageName);
        }
        return iconPackResources;
    }

    public void unloadIconPack() {
        mLoadedIconPackResource = null;
        mLoadedIconPackName = null;
        mIconPackResources = null;
        mIconMasks = null;
        mIconBacks = null;
        mIconUpons = null;
        iconMaskList = null;
        iconBackList = null;
        iconUponList = null;
        mIconScale = 1f;
    }

    public static void pickIconPack(final Context context, final boolean pickIcon) {
        Map<String, IconPackInfo> supportedPackages = getSupportedPackages(context);
        final IconAdapter adapter = new IconAdapter(context, supportedPackages);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_pick_iconpack_title);
        if (!pickIcon) {
            builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {
                    if (adapter.isPlayStoreIconPack(position)) {
                        if (!PackageUtils.doesPackageExist(context.getPackageManager(), HOME_THEME_PACK_PACKAGE_NAME)) {
                            PlayStoreUtils.launchPlayStore(context, HOME_THEME_PACK_PACKAGE_NAME);
                        } else {
                            IntentUtils.launchUrl(context, "https://play.google.com/store/search?q=apex%20nova%20adw%20go%20theme&c=apps");
                        }
                        return;
                    }
                    if (adapter.isCurrentIconPack(position)) {
                        return;
                    }
                    String selectedPackage = adapter.getItem(position);

                    SettingsProvider.putString(context,
                            SettingsProvider.SETTINGS_UI_GENERAL_ICONS_ICON_PACK, selectedPackage);
                    String activityName = adapter.getActivityName(position);
                    if (activityName != null && !TextUtils.isEmpty(activityName)) {
                        SettingsProvider.putString(context,
                               SettingsProvider.SETTINGS_UI_GENERAL_ICONS_ICON_PACK_LENNOX, activityName);
                    } else {
                        SettingsProvider.putString(context,
                               SettingsProvider.SETTINGS_UI_GENERAL_ICONS_ICON_PACK_LENNOX, "");
                    }
                    LauncherAppState.getInstance().getIconCache().flush();
                    LauncherAppState.getInstance().getModel().forceReload();
                }
            });
        } else {
            builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    String selectedPackage = adapter.getItem(which);
                    Launcher launcherActivity = (Launcher) context;
                    if (TextUtils.isEmpty(selectedPackage)) {
                        launcherActivity.onActivityResult(Launcher.REQUEST_PICK_ICON, Activity.RESULT_OK, null);
                    } else {
                        Intent i = new Intent();
                        i.setClass(context, IconPickerActivity.class);
                        i.putExtra(IconPickerActivity.PACKAGE_NAME_EXTRA, selectedPackage);
                        launcherActivity.startActivityForResult(i, Launcher.REQUEST_PICK_ICON);
                    }
                }
            });
        }
        builder.show();
    }

    boolean isIconPackLoaded() {
        return mLoadedIconPackResource != null &&
                mLoadedIconPackName != null &&
                mIconPackResources != null;
    }

    private int getResourceIdForDrawable(String resource) {
        int resId = mLoadedIconPackResource.getIdentifier(resource, "drawable", mLoadedIconPackName);
        return resId;
    }

    public Resources getIconPackResources() {
        return mLoadedIconPackResource;
    }

    public int getResourceIdForActivityIcon(ActivityInfo info) {
        String drawable = mIconPackResources.get(info.packageName.toLowerCase()
                + "." + info.name.toLowerCase());
        if (drawable == null) {
            // Icon pack doesn't have an icon for the activity, fallback to package icon
            drawable = mIconPackResources.get(info.packageName.toLowerCase());
            if (drawable == null) {
                return 0;
            }
        }
        return getResourceIdForDrawable(drawable);
    }

    static class IconPackInfo {
        String packageName;
        CharSequence label;
        Drawable icon;

        IconPackInfo(ResolveInfo r, PackageManager packageManager) {
            packageName = r.activityInfo.packageName;
            icon = r.loadIcon(packageManager);
            label = r.loadLabel(packageManager);
        }

        IconPackInfo(ResolveInfo r, PackageManager packageManager, String activityName) {
            packageName = r.activityInfo.packageName + "|" + activityName;
            icon = r.loadIcon(packageManager);
            label = r.loadLabel(packageManager);
        }

        IconPackInfo(){
        }

        public IconPackInfo(String label, Drawable icon, String packageName) {
            this.label = label;
            this.icon = icon;
            this.packageName = packageName;
        }
    }

    private static class IconAdapter extends BaseAdapter {
        ArrayList<IconPackInfo> mSupportedPackages;
        LayoutInflater mLayoutInflater;
        String mCurrentIconPack, mCurrentIconPackLennox;
        int mCurrentIconPackPosition = -1;
        private boolean mAmazonStore;

        IconAdapter(Context ctx, Map<String, IconPackInfo> supportedPackages) {
            mLayoutInflater = LayoutInflater.from(ctx);
            mSupportedPackages = new ArrayList<IconPackInfo>();
            if (!supportedPackages.isEmpty()) {
                mSupportedPackages = new ArrayList<IconPackInfo>(supportedPackages.values());
                Collections.sort(mSupportedPackages, new Comparator<IconPackInfo>() {
                    @Override
                    public int compare(IconPackInfo lhs, IconPackInfo rhs) {
                        return lhs.label.toString().compareToIgnoreCase(rhs.label.toString());
                    }
                });
            }

            Resources res = ctx.getResources();
            String defaultLabel = res.getString(R.string.default_iconpack_title);
            Drawable icon = res.getDrawable(R.drawable.ic_launcher);
            mSupportedPackages.add(0, new IconPackInfo(defaultLabel, icon, ""));

            mAmazonStore = PlayStoreUtils.installedFromAmazon(ctx);
            if (!mAmazonStore) {
                String moreLabel = res.getString(R.string.more_iconpack_title);
                icon = res.getDrawable(R.mipmap.ic_launcher_play_store);
                mSupportedPackages.add(new IconPackInfo(moreLabel, icon, ""));
            }

            String defaultIconPack = FileUtils.getFileProp(LauncherProvider.OVERRIDES_FILE,
                    "default_theme", "com.lennox.theme");
            mCurrentIconPack = SettingsProvider.getStringCustomDefault(ctx,
                  SettingsProvider.SETTINGS_UI_GENERAL_ICONS_ICON_PACK, defaultIconPack);
            mCurrentIconPackLennox = SettingsProvider.getStringCustomDefault(ctx,
                  SettingsProvider.SETTINGS_UI_GENERAL_ICONS_ICON_PACK_LENNOX, "");
        }

        @Override
        public int getCount() {
            return mSupportedPackages.size();
        }

        @Override
        public String getItem(int position) {
            return mSupportedPackages.get(position).packageName.split("\\|", -1)[0];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public boolean isCurrentIconPack(int position) {
            return mCurrentIconPackPosition == position;
        }

        public boolean isPlayStoreIconPack(int position) {
            return ((!mAmazonStore) && getCount()-1 == position);
        }

        public String getActivityName(int position) {
            try {
                return mSupportedPackages.get(position).packageName.split("\\|", -1)[1];
            } catch (Exception e) {
                return "";
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.iconpack_chooser, null);
            }
            IconPackInfo info = mSupportedPackages.get(position);
            TextView txtView = (TextView) convertView.findViewById(R.id.title);
            txtView.setText(info.label);
            ImageView imgView = (ImageView) convertView.findViewById(R.id.icon);
            imgView.setImageDrawable(info.icon);
            RadioButton radioButton = (RadioButton) convertView.findViewById(R.id.radio);
            boolean isCurrentIconPack;
            if (!TextUtils.isEmpty(mCurrentIconPackLennox) && getActivityName(position) != null) {
                isCurrentIconPack = (getItem(position).equals(mCurrentIconPack) &&
                             getActivityName(position).equals(mCurrentIconPackLennox));
            } else {
                isCurrentIconPack = getItem(position).equals(mCurrentIconPack);
            }
            boolean isMoreIconPack = isPlayStoreIconPack(position);
            radioButton.setChecked(isCurrentIconPack && !isMoreIconPack);
            if (isCurrentIconPack) {
                mCurrentIconPackPosition = position;
            }
            return convertView;
        }
    }

}