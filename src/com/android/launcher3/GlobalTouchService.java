/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.launcher3;

import android.app.ActivityOptions;
import android.app.Service;
import android.content.*;
import android.content.Intent.ShortcutIconResource;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.View;

import com.lennox.launcher3.settings.SettingsProvider;

public class GlobalTouchService extends Service implements View.OnTouchListener,
          SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "GlobalTouchService";

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 500;

    private float mDownX = -1;
    private float mDownY = -1;
    private long mDownTime = 0;

    private GlobalTouchServiceView mView;

    private SharedPreferences mSettings;

    private boolean mLeft;
    private boolean mBackground;
    private boolean mSettingsMode;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            mLeft = intent.getBooleanExtra("swipedLeft", false);
            mBackground = intent.getBooleanExtra("showBackground", false);
            mSettingsMode = intent.getBooleanExtra("settingsMode", false);
        } else {
            mLeft = false;
            mBackground = false;
            mSettingsMode = false;
        }

        mSettings = getSharedPreferences(SettingsProvider.SETTINGS_KEY,
                Context.MODE_PRIVATE);
        mSettings.registerOnSharedPreferenceChangeListener(this);

        makeBarView();
        return super.onStartCommand(intent, flags, startId);
    }

    private void makeBarView() {
        if (mView != null) {
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(mView);
            mView = null;
        }
        mView = new GlobalTouchServiceView(this);
        if (mBackground) {
            int background = mSettingsMode ? R.color.bar_customise_color : R.color.wallpaper_picker_translucent_gray;
            mView.setBackgroundResource(background);
        }

        float widthPref = getBarWidthSetting(mSettings);
        float heightPref = getBarHeightSetting(mSettings);
        int barGravity = getBarGravitySetting(mSettings);

        if (!mSettingsMode) mView.setOnTouchListener(this);
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        int width = (int) ((float) metrics.widthPixels * widthPref);
        int height = (int) ((float) metrics.heightPixels * heightPref);
        WindowManager.LayoutParams params =
            new WindowManager.LayoutParams(width, height,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);

        if (mLeft) {
            params.gravity = Gravity.RIGHT | barGravity;
        } else {
            params.gravity = Gravity.LEFT | barGravity;
        }
        params.setTitle(TAG);
        wm.addView(mView, params);
    }

    @Override
    public boolean onTouch(View v, MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            /*mView.setLayoutParams(new WindowManager.LayoutParams(
                WindowManager.LayoutParams.FILL_PARENT,
                WindowManager.LayoutParams.FILL_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT));*/
            Log.d(TAG, "Touch down location: " + ev.getX() + ", " + ev.getY());
            mDownTime = System.currentTimeMillis();
            mDownX = ev.getX();
            mDownY = ev.getY();
        } else if (ev.getAction() == MotionEvent.ACTION_UP && mDownX != -1 && mDownY != -1 && mDownTime != 0) {
            Log.d(TAG, "Touch up location: " + ev.getX() + ", " + ev.getY());
            boolean result;
            try {
                float diffY = ev.getY() - mDownY;
                float diffX = ev.getX() - mDownX;
                long swipeTime = System.currentTimeMillis() - mDownTime;
                Log.d(TAG, "Swipe time: " + swipeTime);
                Log.d(TAG, "Movement: " + diffX + ", " + diffY);
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD
                            && swipeTime < SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            result = onSwipeRight();
                        } else {
                            result = onSwipeLeft();
                        }
                    }
                } else {
                    if (Math.abs(diffY) > SWIPE_THRESHOLD
                            && swipeTime < SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            result = onSwipeBottom();
                        } else {
                            result = onSwipeTop();
                        }
                    }
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            mDownX = -1;
            mDownY = -1;
            mDownTime = 0;
        }
        return false;
    }

    public boolean onSwipeRight() {
        Log.d(TAG, "Is swipe right");
        if (!mLeft) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle options = ActivityOptions.makeCustomAnimation(this, R.anim.slide_in_from_left, R.anim.slide_out_to_right).toBundle();
            startActivity(startMain, options);
        }
        return false;
    }
 
    public boolean onSwipeLeft() {
        Log.d(TAG, "Is swipe left");
        if (mLeft) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle options = ActivityOptions.makeCustomAnimation(this, R.anim.slide_in_from_right, R.anim.slide_out_to_left).toBundle();
            startActivity(startMain, options);
        }
        return false;
    }
 
    public boolean onSwipeTop() {
        Log.d(TAG, "Is swipe top");
        return false;
    }
 
    public boolean onSwipeBottom() {
        Log.d(TAG, "Is swipe bottom");
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mView != null) {
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(mView);
            mView = null;
        }
        mSettings.unregisterOnSharedPreferenceChangeListener(this);
    }

    private int getBarGravitySetting(SharedPreferences prefs) {
        String newValue = prefs.getString(SettingsProvider.SETTINGS_UI_HOMESCREEN_SWIPE_BAR_GRAVITY,
                                    getString(R.string.preferences_interface_homescreen_swipe_bar_gravity_default));
        if (newValue.equals("top")) {
            return Gravity.TOP;
        } else if (newValue.equals("bottom")) {
            return Gravity.BOTTOM;
        } else {
            return Gravity.CENTER_VERTICAL;
        }
    }

    private float getBarHeightSetting(SharedPreferences prefs) {
        String newValue = prefs.getString(SettingsProvider.SETTINGS_UI_HOMESCREEN_SWIPE_BAR_HEIGHT,
                                    getString(R.string.preferences_interface_homescreen_swipe_bar_height_default));
        int percent = Integer.parseInt(newValue);
        return (float) ((float) percent / 100f);
    }

    private float getBarWidthSetting(SharedPreferences prefs) {
        String newValue = prefs.getString(SettingsProvider.SETTINGS_UI_HOMESCREEN_SWIPE_BAR_WIDTH,
                                    getString(R.string.preferences_interface_homescreen_swipe_bar_width_default));
        int percent = Integer.parseInt(newValue);
        return (float) ((float) percent / 100f);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsProvider.SETTINGS_UI_HOMESCREEN_SWIPE_BAR_WIDTH)) {
            makeBarView();
        } else if (key.equals(SettingsProvider.SETTINGS_UI_HOMESCREEN_SWIPE_BAR_HEIGHT)) {
            makeBarView();
        } else if (key.equals(SettingsProvider.SETTINGS_UI_HOMESCREEN_SWIPE_BAR_GRAVITY)) {
            makeBarView();
        }
    }

    class GlobalTouchServiceView extends ViewGroup {
        public GlobalTouchServiceView(Context context) {
            super(context);
        }
        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
        }
        @Override
        protected void onLayout(boolean arg0, int arg1, int arg2, int arg3, int arg4) {
        }
    }

}

