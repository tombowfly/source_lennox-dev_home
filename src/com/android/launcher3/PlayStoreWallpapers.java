package com.lennox.launcher3;

import android.os.Bundle;

import com.lennox.utils.aliases.AliasActivity;
import com.lennox.utils.IntentUtils;
import com.lennox.utils.PackageUtils;
import com.lennox.utils.PlayStoreUtils;

public class PlayStoreWallpapers extends AliasActivity {

    public static String PACKAGE_NAME = "com.lennox.wallpapers";

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        if (!PackageUtils.doesPackageExist(getPackageManager(), PlayStoreWallpapers.PACKAGE_NAME)) {
            PlayStoreUtils.launchPlayStore(this, PACKAGE_NAME);
        } else {
            IntentUtils.launchUrl(this, "https://play.google.com/store/search?q=wallpaper%20collection&c=apps");
        }
    }
}
