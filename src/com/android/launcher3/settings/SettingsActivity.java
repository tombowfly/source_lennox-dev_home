/*
 * Copyright (C) 2013 The CyanogenMod Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.launcher3.settings;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.*;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.lennox.launcher3.R;
import com.lennox.launcher3.HotwordCustomFragment;
import java.util.ArrayList;
import java.util.List;

import com.lennox.launcher3.IconPackHelper;

import com.lennox.preferences.PreferenceUtils;

import com.pollfish.interfaces.PollfishSurveyReceivedListener;
import com.pollfish.interfaces.PollfishSurveyNotAvailableListener;
import com.pollfish.interfaces.PollfishSurveyCompletedListener;
import com.pollfish.interfaces.PollfishOpenedListener;
import com.pollfish.interfaces.PollfishClosedListener;
import com.lennox.pollfish.PollFishHelper;

import com.google.ads.AdsHelper;

public class SettingsActivity extends PreferenceActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener,
        PollfishSurveyReceivedListener, PollfishOpenedListener,
        PollfishSurveyNotAvailableListener, PollfishSurveyCompletedListener, PollfishClosedListener {

    private static final String TAG = "Launcher3.SettingsActivity";

    private SharedPreferences mSettings;

    private static String sCurrentKey = "";

    private static final int REQUEST_PICK_APPLICATION = 6;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PollFishHelper.onCreate(this);
        AdsHelper.onCreate(this);

        mSettings = getSharedPreferences(SettingsProvider.SETTINGS_KEY,
                Context.MODE_PRIVATE);

        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public SharedPreferences getSharedPreferences(String name, int mode) {
        return super.getSharedPreferences(SettingsProvider.SETTINGS_KEY,
                Context.MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PollFishHelper.onResume(this);
        AdsHelper.onResume(this);
        mSettings.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onHeaderClick(Header header, int position) {
        AdsHelper.onResume(this);
        super.onHeaderClick(header, position);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSettings.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onBackPressed() {
        AdsHelper.onBackPressed(this);
    }

    @Override
    public boolean isValidFragment(String fragmentName) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.preferences_headers, target);
        PreferenceUtils.setAboutHeader(target, this);
        AdsHelper.adBanner(this, true);
    }

    @Override
    public void setListAdapter(ListAdapter adapter) {
        if (adapter == null) {
            super.setListAdapter(null);
        } else {
            List<Header> headers = getHeadersFromAdapter(adapter);
            super.setListAdapter(new HeaderAdapter(this, headers));
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(SettingsProvider.SETTINGS_CHANGED, true);
        editor.commit();
    }

    private List<Header> getHeadersFromAdapter(ListAdapter adapter) {
        List<Header> headers = new ArrayList<Header>();
        int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            headers.add((Header)adapter.getItem(i));
        }
        return headers;
    }
 
    public static class HomescreenFragment extends PreferenceFragment
            implements Preference.OnPreferenceChangeListener {

        private ListPreference mTransitionEffect;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences_homescreen);

            PreferenceScreen prefSet = getPreferenceScreen();

            mTransitionEffect = (ListPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_HOMESCREEN_SCROLLING_TRANSITION_EFFECT);
            mTransitionEffect.setOnPreferenceChangeListener(this);
            mTransitionEffect.setSummary(mTransitionEffect.getEntries()[mTransitionEffect.findIndexOfValue(mTransitionEffect.getValue())]);
        }

        @Override
        public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
            String key = preference.getKey();
            if (key.equals(SettingsProvider.SETTINGS_UI_HOMESCREEN_SWIPE_LEFT_APP) ||
                key.equals(SettingsProvider.SETTINGS_UI_HOMESCREEN_SWIPE_RIGHT_APP)) {
                pickApplication(getActivity(), key);
                return true;
            }
            return false;
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if ( preference.getKey().equals(SettingsProvider.SETTINGS_UI_HOMESCREEN_SCROLLING_TRANSITION_EFFECT)) {
                mTransitionEffect.setSummary(mTransitionEffect.getEntries()[mTransitionEffect.findIndexOfValue((String) newValue)]);
                return true;
            }
            return false;
        }
    }

    public static class DrawerFragment extends PreferenceFragment
            implements Preference.OnPreferenceChangeListener {

        private ListPreference mTransitionEffect;
        private ListPreference mSortMode;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences_drawer);

            PreferenceScreen prefSet = getPreferenceScreen();

            mTransitionEffect = (ListPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_DRAWER_SCROLLING_TRANSITION_EFFECT);
            mTransitionEffect.setOnPreferenceChangeListener(this);
            mTransitionEffect.setSummary(mTransitionEffect.getEntries()[mTransitionEffect.findIndexOfValue(mTransitionEffect.getValue())]);
            mSortMode = (ListPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_DRAWER_SORT_MODE);
            mSortMode.setOnPreferenceChangeListener(this);
            mSortMode.setSummary(mSortMode.getEntries()[mSortMode.findIndexOfValue(mSortMode.getValue())]);
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if ( preference.getKey().equals(SettingsProvider.SETTINGS_UI_DRAWER_SCROLLING_TRANSITION_EFFECT)) {
                mTransitionEffect.setSummary(mTransitionEffect.getEntries()[mTransitionEffect.findIndexOfValue((String) newValue)]);
                return true;
            } else if ( preference.getKey().equals(SettingsProvider.SETTINGS_UI_DRAWER_SORT_MODE)) {
                mSortMode.setSummary(mSortMode.getEntries()[mSortMode.findIndexOfValue((String) newValue)]);
                return true;
            }
            return false;
        }

    }

    public static class DockFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences_dock);
        }
    }

    public static class GesturesFragment extends PreferenceFragment
            implements Preference.OnPreferenceChangeListener {
        private ListPreference mHomescreenDoubleTap;
        private ListPreference mHomescreenSwipeUp;
        private ListPreference mHomescreenSwipeDown;
        private ListPreference mHomescreenPinch;
        private ListPreference mHomescreenSpread;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences_gestures);

            PreferenceScreen prefSet = getPreferenceScreen();

            mHomescreenDoubleTap = (ListPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_GESTURE_DOUBLE_TAP);
            mHomescreenDoubleTap.setOnPreferenceChangeListener(this);
            initGesturePreference(mHomescreenDoubleTap);
            mHomescreenDoubleTap.setSummary(mHomescreenDoubleTap.getEntry());
            mHomescreenSwipeDown = (ListPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_GESTURE_DOWN);
            mHomescreenSwipeDown.setOnPreferenceChangeListener(this);
            initGesturePreference(mHomescreenSwipeDown);
            mHomescreenSwipeDown.setSummary(mHomescreenSwipeDown.getEntry());
            mHomescreenSwipeUp = (ListPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_GESTURE_UP);
            mHomescreenSwipeUp.setOnPreferenceChangeListener(this);
            initGesturePreference(mHomescreenSwipeUp);
            mHomescreenSwipeUp.setSummary(mHomescreenSwipeUp.getEntry());
            mHomescreenSpread = (ListPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_GESTURE_SPREAD);
            mHomescreenSpread.setOnPreferenceChangeListener(this);
            initGesturePreference(mHomescreenSpread);
            mHomescreenSpread.setSummary(mHomescreenSpread.getEntry());
            mHomescreenPinch = (ListPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_GESTURE_PINCH);
            mHomescreenPinch.setOnPreferenceChangeListener(this);
            initGesturePreference(mHomescreenPinch);
            mHomescreenPinch.setSummary(mHomescreenPinch.getEntry());
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if ( preference instanceof ListPreference ) {
                ListPreference gesturePref = (ListPreference)preference;
                gesturePref.setSummary(gesturePref.getEntries()[gesturePref.findIndexOfValue((String) newValue)]);
                return true;
            }
            return false;
        }

        private void initGesturePreference(ListPreference preference) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                preference.setEntries(getResources().getStringArray(R.array.gesture_target_names_pre42));
                preference.setEntryValues(getResources().getStringArray(R.array.gesture_target_values_pre42));
            }
        }
    }

    public static class GeneralFragment extends PreferenceFragment
            implements Preference.OnPreferenceChangeListener {

        public static final String KEY_ENABLE_HOTWORD = "pref_key_enableHotword";
        public static final String KEY_CUSTOM_HOTWORDS = "pref_key_customHotwords";

        private CheckBoxPreference miPhoneMode;
        private CheckBoxPreference mAllowDuplicates;
        private CheckBoxPreference mEnableAllApps;

        private Preference mCustomHotwords;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences_general);

            PreferenceScreen prefSet = getPreferenceScreen();

            boolean iPhoneMode = SettingsProvider.getBoolean(getActivity(), SettingsProvider.SETTINGS_UI_GENERAL_IPHONE_MODE,
                  R.bool.preferences_interface_general_iphone_mode_default);

            mCustomHotwords = prefSet.findPreference(KEY_CUSTOM_HOTWORDS);

            miPhoneMode = (CheckBoxPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_GENERAL_IPHONE_MODE);
            miPhoneMode.setOnPreferenceChangeListener(this);
            mAllowDuplicates = (CheckBoxPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_GENERAL_IPHONE_ALLOW_DUPLICATES);
            mEnableAllApps = (CheckBoxPreference) prefSet.findPreference(SettingsProvider.SETTINGS_UI_GENERAL_IPHONE_ENABLE_ALLAPPS);
            if (!iPhoneMode) {
                mAllowDuplicates.setEnabled(false);
                mAllowDuplicates.setSummary(R.string.pref_disabled_allapps_mode);
                mEnableAllApps.setEnabled(false);
                mEnableAllApps.setSummary(R.string.pref_disabled_allapps_mode);
            }
        }

        @Override
        public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
            if (preference.getKey().equals(SettingsProvider.SETTINGS_UI_GENERAL_ICONS_ICON_PACK)) {
                IconPackHelper.pickIconPack(getActivity(), false);
                return true;
            } else if (preference == mCustomHotwords) {
                getFragmentManager().beginTransaction()
                    .replace(android.R.id.content, new HotwordCustomFragment())
                    .hide(this)
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .setBreadCrumbTitle(R.string.pref_hotwords_title)
                    .commit();
            }
            return false;
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if ( preference.getKey().equals(SettingsProvider.SETTINGS_UI_GENERAL_IPHONE_MODE)) {
                boolean iPhoneMode = (Boolean) newValue;
                int summaryDup = iPhoneMode ?
                      R.string.preferences_interface_general_iphone_allow_duplicates_summary :
                      R.string.pref_disabled_allapps_mode;
                int summaryAll = iPhoneMode ?
                      R.string.preferences_interface_general_iphone_enable_allapps_summary :
                      R.string.pref_disabled_allapps_mode;
                mAllowDuplicates.setEnabled(iPhoneMode);
                mAllowDuplicates.setSummary(summaryDup);
                mEnableAllApps.setEnabled(iPhoneMode);
                mEnableAllApps.setSummary(summaryAll);
                return true;
            }
            return false;
        }

    }

    private static class HeaderAdapter extends ArrayAdapter<Header> {
        private static final int HEADER_TYPE_NORMAL = 0;
        private static final int HEADER_TYPE_CATEGORY = 1;

        private static final int HEADER_TYPE_COUNT = HEADER_TYPE_CATEGORY + 1;

        private static class HeaderViewHolder {
            ImageView icon;
            TextView title;
            TextView summary;
        }

        private LayoutInflater mInflater;

        static int getHeaderType(Header header) {
            if (header.fragment == null && header.intent == null) {
                return HEADER_TYPE_CATEGORY;
            } else {
                return HEADER_TYPE_NORMAL;
            }
        }

        @Override
        public int getItemViewType(int position) {
            Header header = getItem(position);
            return getHeaderType(header);
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false; // because of categories
        }

        @Override
        public boolean isEnabled(int position) {
            return getItemViewType(position) != HEADER_TYPE_CATEGORY;
        }

        @Override
        public int getViewTypeCount() {
            return HEADER_TYPE_COUNT;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        public HeaderAdapter(Context context, List<Header> objects) {
            super(context, 0, objects);

            mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            HeaderViewHolder holder;
            Header header = getItem(position);
            int headerType = getHeaderType(header);
            View view = null;

            if (convertView == null) {
                holder = new HeaderViewHolder();
                switch (headerType) {
                    case HEADER_TYPE_CATEGORY:
                        view = mInflater.inflate(
                                R.layout.preference_header_category, parent,
                                false);
                        holder.title = (TextView)
                                view.findViewById(android.R.id.title);
                        break;

                    case HEADER_TYPE_NORMAL:
                        view = mInflater.inflate(
                                R.layout.preference_header_item, parent,
                                false);
                        holder.icon = (ImageView) view.findViewById(android.R.id.icon);
                        holder.title = (TextView)
                                view.findViewById(android.R.id.title);
                        holder.summary = (TextView)
                                view.findViewById(android.R.id.summary);
                        break;
                }
                view.setTag(holder);
            } else {
                view = convertView;
                holder = (HeaderViewHolder) view.getTag();
            }

            // All view fields must be updated every time, because the view may be recycled
            switch (headerType) {
                case HEADER_TYPE_CATEGORY:
                    holder.title.setText(header.getTitle(getContext().getResources()));
                    break;

                case HEADER_TYPE_NORMAL:
                    holder.icon.setImageResource(header.iconRes);
                    holder.title.setText(header.getTitle(getContext().getResources()));
                    CharSequence summary = header.getSummary(getContext().getResources());
                    if (!TextUtils.isEmpty(summary)) {
                        holder.summary.setVisibility(View.VISIBLE);
                        holder.summary.setText(summary);
                    } else {
                        holder.summary.setVisibility(View.GONE);
                    }
                    break;
            }

            return view;
        }
    }

    @Override
    protected void onActivityResult(
            final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == REQUEST_PICK_APPLICATION && data != null) {
            if (resultCode == RESULT_OK && !sCurrentKey.equals("")) {
                String component = data.getComponent().flattenToString();
                if (component.equals("")) component = getString(R.string.preferences_interface_homescreen_swipe_left_app_default);
                SharedPreferences.Editor editor = mSettings.edit();
                editor.putString(sCurrentKey, component);
                editor.putBoolean(SettingsProvider.SETTINGS_CHANGED, true);
                editor.commit();
            }
            return;
        }
        sCurrentKey = "";
    }

    static void pickApplication(Activity activity, String key) {
        sCurrentKey = key;

        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        Intent pickIntent = new Intent("com.lennox.launcher3.PICK_APPLICATION");
        pickIntent.putExtra(Intent.EXTRA_INTENT, mainIntent);
        pickIntent.putExtra(Intent.EXTRA_TITLE, activity.getText(R.string.title_select_application));
        activity.startActivityForResult(pickIntent, REQUEST_PICK_APPLICATION);
    }

    // PollFish Helpers

    @Override
    public void onPollfishSurveyReceived(boolean shortSurveys, int surveyPrice) {
        PollFishHelper.onPollfishSurveyReceived(this, shortSurveys, surveyPrice);
    }

    @Override
    public void onPollfishSurveyNotAvailable() {
        PollFishHelper.onPollfishSurveyNotAvailable(this);
    }

    @Override
    public void onPollfishSurveyCompleted(boolean shortSurveys , int surveyPrice) {
        PollFishHelper.onPollfishSurveyCompleted(this, shortSurveys, surveyPrice);
    }

    @Override
    public void onPollfishOpened () {
        PollFishHelper.onPollfishOpened(this);
    }

    @Override
    public void onPollfishClosed () {
        PollFishHelper.onPollfishClosed(this);
    }

}
