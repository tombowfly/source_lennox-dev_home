/*
 * Copyright (C) 2013 The CyanogenMod Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.launcher3.settings;

import android.content.Context;
import android.content.SharedPreferences;

public final class SettingsProvider {
    public static final String SETTINGS_KEY = "launcher_preferences";

    public static final String SETTINGS_CHANGED = "settings_changed";

    public static final String SETTINGS_UI_HOMESCREEN_DEFAULT_SCREEN_ID = "ui_homescreen_default_screen_id";
    public static final String SETTINGS_UI_HOMESCREEN_ICON_SCALE = "ui_homescreen_icon_scale";
    public static final String SETTINGS_UI_HOMESCREEN_SEARCH = "ui_homescreen_search";
    public static final String SETTINGS_UI_HOMESCREEN_HIDE_ICON_LABELS = "ui_homescreen_general_hide_icon_labels";
    public static final String SETTINGS_UI_HOMESCREEN_SWIPE_LEFT_ENABLE = "ui_homescreen_swipe_left_enable";
    public static final String SETTINGS_UI_HOMESCREEN_SWIPE_LEFT_APP = "ui_homescreen_swipe_left_app";
    public static final String SETTINGS_UI_HOMESCREEN_SWIPE_RIGHT_ENABLE = "ui_homescreen_swipe_right_enable";
    public static final String SETTINGS_UI_HOMESCREEN_SWIPE_RIGHT_APP = "ui_homescreen_swipe_right_app";
    public static final String SETTINGS_UI_HOMESCREEN_SWIPE_SERVICE_ENABLE = "ui_homescreen_swipe_service_enable";
    public static final String SETTINGS_UI_HOMESCREEN_SWIPE_SERVICE_SHOW = "ui_homescreen_swipe_service_show";
    public static final String SETTINGS_UI_HOMESCREEN_SWIPE_BAR_HEIGHT = "ui_homescreen_swipe_bar_height";
    public static final String SETTINGS_UI_HOMESCREEN_SWIPE_BAR_WIDTH = "ui_homescreen_swipe_bar_width";
    public static final String SETTINGS_UI_HOMESCREEN_SWIPE_BAR_GRAVITY = "ui_homescreen_swipe_bar_gravity";
    public static final String SETTINGS_UI_HOMESCREEN_DISABLE_SHORTCUTS = "ui_homescreen_disable_shortcuts";
    public static final String SETTINGS_UI_HOMESCREEN_SCROLLING_TRANSITION_EFFECT = "ui_homescreen_scrolling_transition_effect";
    public static final String SETTINGS_UI_HOMESCREEN_SCROLLING_WALLPAPER_SCROLL = "ui_homescreen_scrolling_wallpaper_scroll";
    public static final String SETTINGS_UI_HOMESCREEN_SCROLLING_PAGE_OUTLINES = "ui_homescreen_scrolling_page_outlines";
    public static final String SETTINGS_UI_HOMESCREEN_SCROLLING_FADE_ADJACENT = "ui_homescreen_scrolling_fade_adjacent";
    public static final String SETTINGS_UI_HOMESCREEN_ROWS = "ui_homescreen_rows";
    public static final String SETTINGS_UI_HOMESCREEN_COLUMNS = "ui_homescreen_columns";
    public static final String SETTINGS_UI_DRAWER_HIDDEN_APPS = "ui_drawer_hidden_apps";
    public static final String SETTINGS_UI_DRAWER_HIDDEN_APPS_SHORTCUTS = "ui_drawer_hidden_apps_shortcuts";
    public static final String SETTINGS_UI_DRAWER_HIDDEN_APPS_WIDGETS = "ui_drawer_hidden_apps_widgets";
    public static final String SETTINGS_UI_DRAWER_HIDE_SYSTEM = "ui_drawer_hide_system";
    public static final String SETTINGS_UI_DRAWER_HIDE_USER = "ui_drawer_hide_user";
    public static final String SETTINGS_UI_DRAWER_HIDE_ICON_LABELS = "ui_drawer_hide_icon_labels";
    public static final String SETTINGS_UI_DOCK_LABELS = "ui_dock_labels";
    public static final String SETTINGS_UI_DOCK_SCALE = "ui_dock_scale";
    public static final String SETTINGS_UI_DOCK_ICON_SCALE = "ui_dock_icon_scale";
    public static final String SETTINGS_UI_DOCK_ICONS = "ui_dock_icons";
    public static final String SETTINGS_UI_DRAWER_ICON_SCALE = "ui_drawer_icon_scale";
    public static final String SETTINGS_UI_DRAWER_SCROLLING_RESET_PAGE = "ui_drawer_scrolling_reset_page";
    public static final String SETTINGS_UI_DRAWER_SCROLLING_TRANSITION_EFFECT = "ui_drawer_scrolling_transition_effect";
    public static final String SETTINGS_UI_DRAWER_SCROLLING_FADE_ADJACENT = "ui_drawer_scrolling_fade_adjacent";
    public static final String SETTINGS_UI_DRAWER_LOLLIPOP = "ui_drawer_lollipop";
    public static final String SETTINGS_UI_DRAWER_SORT_MODE = "ui_drawer_sort_mode_string";
    public static final String SETTINGS_UI_DRAWER_REVERSE_SORT = "ui_drawer_reverse_sort";
    public static final String SETTINGS_UI_DRAWER_ROWS = "ui_drawer_rows";
    public static final String SETTINGS_UI_DRAWER_COLUMNS = "ui_drawer_columns";
    public static final String SETTINGS_UI_GESTURE_UP = "ui_gesture_up";
    public static final String SETTINGS_UI_GESTURE_DOWN = "ui_gesture_down";
    public static final String SETTINGS_UI_GESTURE_PINCH = "ui_gesture_pinch";
    public static final String SETTINGS_UI_GESTURE_SPREAD = "ui_gesture_spread";
    public static final String SETTINGS_UI_GESTURE_DOUBLE_TAP = "ui_gesture_double_tap";
    public static final String SETTINGS_UI_GENERAL_IPHONE_MODE = "ui_general_iphone_mode";
    public static final String SETTINGS_UI_GENERAL_IPHONE_ALLOW_DUPLICATES = "ui_general_iphone_allow_duplicates";
    public static final String SETTINGS_UI_GENERAL_IPHONE_ENABLE_ALLAPPS = "ui_general_iphone_enable_allapps";
    public static final String SETTINGS_UI_GENERAL_ICON_SCALE = "ui_general_icon_scale";
    public static final String SETTINGS_UI_GENERAL_ICONS_TEXT_FONT_FAMILY = "ui_general_icons_text_font";
    public static final String SETTINGS_UI_GENERAL_ICONS_TEXT_FONT_STYLE = "ui_general_icons_text_font_style";
    public static final String SETTINGS_UI_GENERAL_ICONS_ICON_PACK = "ui_general_iconpack";
    public static final String SETTINGS_UI_GENERAL_ICONS_ICON_PACK_LENNOX = "ui_general_iconpack_lennox";
    public static final String SETTINGS_UI_GENERAL_ORIENTATION = "ui_general_orientation";
    public static final String SETTINGS_UI_GENERAL_FULLSCREEN = "ui_general_fullscreen";
    public static final String SETTINGS_UI_GENERAL_LOCK_WORKSPACE = "ui_general_lock_workspace";
    public static final String SETTINGS_UI_GENERAL_LOCK_WORKSPACE_TOAST = "ui_general_lock_workspace_toast";

    public static SharedPreferences get(Context context) {
        return context.getSharedPreferences(SETTINGS_KEY, Context.MODE_MULTI_PROCESS);
    }

    public static int getIntCustomDefault(Context context, String key, int def) {
        return get(context).getInt(key, def);
    }

    public static int getInt(Context context, String key, int resource) {
        return getIntCustomDefault(context, key, context.getResources().getInteger(resource));
    }

    public static void putInt(Context context, String key, int value) {
        get(context).edit().putInt(key, value).commit();
    }

    public static long getLongCustomDefault(Context context, String key, long def) {
        return get(context).getLong(key, def);
    }

    public static long getLong(Context context, String key, int resource) {
        return getLongCustomDefault(context, key, context.getResources().getInteger(resource));
    }

    public static boolean getBooleanCustomDefault(Context context, String key, boolean def) {
        return get(context).getBoolean(key, def);
    }

    public static boolean getBoolean(Context context, String key, int resource) {
        return getBooleanCustomDefault(context, key, context.getResources().getBoolean(resource));
    }

    public static void putBoolean(Context context, String key, boolean value) {
        get(context).edit().putBoolean(key, value).commit();
    }

    public static String getStringCustomDefault(Context context, String key, String def) {
        return get(context).getString(key, def);
    }

    public static String getString(Context context, String key, int resource) {
        return getStringCustomDefault(context, key, context.getResources().getString(resource));
    }

    public static void putString(Context context, String key, String value) {
        get(context).edit().putString(key, value).commit();
    }
}
